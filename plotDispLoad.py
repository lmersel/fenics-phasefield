import numpy as np
import matplotlib.pyplot as plt


# Literature review
Data_Hesch = np.loadtxt('Hesch2014SENS.txt')
Data_Borden = np.loadtxt('Borden2012.txt')

plt.figure(1)
plt.plot(Data_Hesch[:,0], Data_Hesch[:,1],'.-', linewidth = 1, label='Hesch - lc = 0.015mm')
plt.plot(Data_Borden[:,0], Data_Borden[:,1],'.-', linewidth = 1, label='Borden - lc = 0.0075mm')
plt.xlabel('Displacement [mm]') ; plt.ylabel('Load [kN]')
plt.legend()



# Simulation result
savedir       = 'pathway of the forder'
solver        = ['RESULT_256x256_at2_dt_0_0001s_corrige3/']#, 'RESULT_256x256_at2_dt_0_0001s_lc2hmin/']
legend_solver = ['lc=4.2h, 256x256']#,'lc=2.8h, 256x256']


# Force Data [N] ==> 0.001*Data [kN]

for i,m in enumerate(solver):
    Data_forces = np.loadtxt(savedir+str(m)+'forces.txt')
    Data_load = np.loadtxt(savedir+str(m)+'loading.txt')
    Data_react = np.loadtxt(savedir+str(m)+'reaction.txt')

    plt.figure(1)

    plt.plot(Data_load[:,1], 0.001*Data_react[:,1], 'o', linewidth = 1, label = 'Rx')
    plt.plot(Data_load[:,1], 0.001*Data_react[:,2], 'o', linewidth = 1, label = 'Ry')
    plt.plot(Data_load[:,1], 0.001*np.sqrt(Data_react[:,1]**2 + Data_react[:,2]**2), 'o', linewidth = 1, label = 'sqrt(Rx²+Ry²)')
    plt.legend()



plt.show()

