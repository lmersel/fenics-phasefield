#from fenics import *
import fenics as fe
import numpy as np
import ufl


ppos = lambda x: (x+np.abs(x))/2.
pneg = lambda x: (x-np.abs(x))/2.
#ppos = lambda x: 1*x + np.abs(x)
#pneg = lambda x: 1*x

def as_2D_tensor(X):
    return fe.as_tensor([[X[0], X[2]],[X[2], X[1]]])
def as_2D_vector(X):
    return fe.as_vector([X[0,0], X[1,1], 2*X[1,0]])

def as_3D_tensor(X):
    return fe.as_tensor([[X[0], X[3], 0],
                      [X[3], X[1], 0],
                      [0, 0, X[2]]])
def trace(X):
    return X[0,0]+ X[1,1]+X[2,2]

def w(alpha):
    return alpha**2


def w_prime(alpha):
    return 2*alpha

#Degradation function
def gk(alpha, k= 1e-6):
    return (1-alpha)**2 + k


def gk_prime(alpha, k= 1e-6):
    return 2*(alpha-1)


# antiplane case
def epsilon(u):
    e = (fe.grad(u) + fe.grad(u).T)/2
    return fe.as_tensor([[e[0,0], e[0,1], 0],
                      [e[1,0], e[1,1], 0],
                       [0     , 0     , 0]])

# def epsilon(u):
#     e = sym(nabla_grad(u))
#     return as_vector([e[0,0], e[1,1], 2*e[1,0]])

def psi_plus(u, K0,G):
    eps = epsilon(u)
    return 0.5* K0*ppos(fe.tr(eps))**2 + G * fe.inner(fe.dev(eps),fe.dev(eps))

def psi_moins(u, K0,G):
    eps = epsilon(u)
    return 0.5* K0*pneg(fe.tr(eps))**2

def psi_0(u, G, K0):
    return  psi_plus(u, K0,G)+psi_moins(u, K0,G)

# def sigma_amor(u, G, K0,ndim):
#     eps = epsilon(u)
#     psi_plus_prime  = 0.5* K0 * ppos(tr(eps)) * Identity(ndim) + 2*G *dev(eps)
#     psi_moins_prime = 0.5* K0 * pneg(tr(eps)) * Identity(ndim)
#     s =  psi_plus_prime + psi_moins_prime
#     return s


##Def without conditional (antiplane case)
def sigma_amor(u, G, K0,ndim):
    eps = epsilon(u)
    psi_plus_prime  = 0.5* K0 * ppos(trace(eps)) * fe.Identity(ndim+1) + 2*G * fe.dev(eps)
    psi_moins_prime = 0.5* K0 * pneg(trace(eps)) * fe.Identity(ndim+1)
    s =  psi_plus_prime + psi_moins_prime
    return fe.as_vector([s[0,0], s[1,1], s[2,2], s[1,0]])



# # Use conditional instead of ufl.Abs()
# def sigma_amor(u, G, K0,ndim):
#     eps = epsilon(u)
#     ppos_tr = fe.conditional(fe.ge(fe.tr(eps),0), fe.tr(eps), 0)
#     pneg_tr = fe.conditional(fe.ge(0,fe.tr(eps)), fe.tr(eps), 0)
#     psi_plus_prime  = 0.5* K0 * ppos_tr * fe.Identity(ndim+1) + 2*G * fe.dev(eps)
#     psi_moins_prime = 0.5* K0 * pneg_tr * fe.Identity(ndim+1)
#     s =  psi_plus_prime + psi_moins_prime
#     return fe.as_vector([s[0,0], s[1,1], s[2,2], s[1,0]])

# # Compute manually the trace to get a value to evaluate  np.abs() instead of ufl.Abs()
# def sigma_amor(u, G, K0,ndim, V_H):
#     eps = epsilon(u)
#     e11 = fe.project(eps[0, 0], V_H).vector().get_local()
#     e22 = fe.project(eps[1, 1], V_H).vector().get_local()
#     e33 = fe.project(eps[2, 2], V_H).vector().get_local()
#     tr_eps = e11 +e22+e33
#     psi_plus_prime  = 0.5* K0 * ppos(tr_eps) * fe.Identity(ndim+1) + 2*G * fe.dev(eps) # Goes wrong tr_esp assess in gauss point and dev(eps) at nodal point
#     psi_moins_prime = 0.5* K0 * pneg(tr_eps) * fe.Identity(ndim+1)
#     s =  psi_plus_prime + psi_moins_prime
#     return fe.as_vector([s[0,0], s[1,1], s[2,2], s[1,0]])

def sigma_bourdin(u, G, K0,ndim):
    eps = epsilon(u)
    return 0.5* K0 * fe.tr(eps) * fe.Identity(ndim) + 2*G * fe.dev(eps)


