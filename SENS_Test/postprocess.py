import numpy as np
import matplotlib.pyplot as plt

"""
=======================================================
         Solver comparison on Bourdin splitting energy
=======================================================
"""
#savedir1 = path_directory
savedir1       = '/scratchm/lmersel/FEniCS/PFmodel/PF_fenics_Lamia/SingleNotchTest/Split_Amor/PetcsSNES/History_modified/'


#solver1 = directory
solver1 = ['results_sh_lw_0_01_h0_0067_Amor_SNES_d_New_u_Hmod/']

#legend name
legend_solver1 = ['H_mod DGspace(for H and sig)']


# Load file in array

for i,m in enumerate(solver1):
    Data_energies = np.loadtxt(savedir1+str(m)+'energies.txt')
    Data_forces = np.loadtxt(savedir1+str(m)+'forces.txt')
    Data_load = np.loadtxt(savedir1+str(m)+'loading.txt')

    plt.figure(1)
    plt.plot(Data_load[:150,1], Data_energies[:150,1], '-', linewidth = 2, label = 'elastic energy ' )   
    plt.plot(Data_load[:150,1], Data_energies[:150,2], '-', linewidth = 2, label = 'dissipted energy ')
    plt.xlabel('Displacement [mm]') ; plt.ylabel('Energy [J]')
    plt.legend()

    plt.figure(2)
    #plt.plot(Data_load[:,1], -Data_forces[:,1], 'b*', linewidth = 2, label = '|Fn|')
    plt.plot(Data_load[:150,1], -Data_forces[:150,2],'-', linewidth = 2, label = '|Ft|')
    plt.plot(Data_load[:150,1], -Data_forces[:150,1],'-', linewidth = 2, label = '|Fn|')
    plt.xlabel('Displacement [mm]') ; plt.ylabel('Force [N]')
    plt.legend()
plt.title('Conjugate Gradient for Mech and Damage ')
plt.show()

