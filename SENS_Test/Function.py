from dolfin import *
from fenics import *
import numpy as np
import ufl



def w(alpha):
    return alpha**2


def w_prime(alpha):
    return 2*alpha

#Degradation function
def gk(alpha, k= 1e-6):
    return (1-alpha)**2 + k


def gk_prime(alpha, k= 1e-6):
    return 2*(alpha-1)

def epsilon(u):
    return sym(nabla_grad(u))

"""

def epsilon_sym(u):
  
  strain_sym = as_tensor([[ u[0].dx(0),                        (1./2.)*(u[0].dx(1) + u[1].dx(0)) ],
                          [ (1./2.)*(u[0].dx(1) + u[1].dx(0)), u[1].dx(1)                        ]])
  
  return strain_sym
"""



"""
def sigma(u_n,u ,g_n , G, K0,ndim):
    Kd = conditional(gt(tr(epsilon(u_n)),0),g_n*K0, K0)
    return  2. * Kd  * tr(epsilon(u))*Identity(ndim) + G * g_n * dev(epsilon(u))
# to avoid the nonlinear form of the stress expression regarding the Kd(tr(eps(u)))*tr(eps(u))
"""


def psi_plus(u, K0,G):
    eps = epsilon(u)
    plus_tr_eps = 0.5*(tr(eps)+abs(tr(eps)))
    return 0.5* K0*(plus_tr_eps)**2 + G *inner(dev(eps),dev(eps))

def psi_moins(u, K0,G):
    eps = epsilon(u)
    moins_tr_eps = 0.5*(tr(eps)-abs(tr(eps)))
    return 0.5* K0*(moins_tr_eps)**2

def psi_0(u, G, K0):
    return  psi_plus(u, K0,G)+psi_moins(u, K0,G)

def psi(u,alpha, G, K0):
    return  gk(alpha, k= 1e-6)*psi_plus(u, K0,G)+psi_moins(u, K0,G)


"""
def sigma_amor(u,alpha , G, K0,ndim):
    eps = epsilon(u)
    Kd = conditional(gt(tr(eps),0),gk(alpha, k= 1e-6)*K0, K0)
    return  2. * Kd  * tr(eps)*Identity(ndim) + G * gk(alpha, k= 1e-6) * dev(eps)
"""
def sigma_amor(u,alpha , G, K0,ndim):
    eps = epsilon(u)
    plus_tr_eps     = 0.5*(tr(eps)+abs(tr(eps)))
    moins_tr_eps    = 0.5*(tr(eps)-abs(tr(eps)))
    psi_plus_prime  = 0.5* K0 * plus_tr_eps * Identity(ndim) + 2*G *dev(eps)
    psi_moins_prime = 0.5* K0 * moins_tr_eps * Identity(ndim)
    return  gk(alpha, k= 1e-6) * psi_plus_prime + psi_moins_prime



"""
def sigma_nl(u,alpha , G, K0,ndim):
    eps = epsilon(u)
    plus_tr_eps = 0.5*(tr(eps)+abs(tr(eps)))
    sig_plus  = K0 * plus_tr_eps  * Identity(ndim)+ 2 * G * dev(eps)
    moins_tr_eps = 0.5*(-tr(eps)+abs(-tr(eps)))
    sig_moins = - K0 * moins_tr_eps * Identity(ndim)
    return   gk(alpha, k= 1e-6) * sig_plus + sig_moins
"""









"""""

import subprocess

text = subprocess.check_output("grep PointData stress000100.vtu", shell = True)
text = text.decode()


to_be_sed = text.split()[1].split("\"")[1] # Recupere la clef f_*****
subprocess.Popen('sed -i s/' + to_be_sed + '/f_0001/g stress000100.test',shell = True) # remplace la clef f_*** par f_0001


text = subprocess.check_output('grep stress\*.vtu',shell = True)

"""""
