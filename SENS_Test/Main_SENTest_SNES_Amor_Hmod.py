from fenics import *
import numpy as np
from Function import *
import matplotlib.pyplot as plt

import sys
import time



"""
*** -------------------------------------------------------------------------
level : 

CRITICAL  = 50, // errors that may lead to data corruption and suchlike
ERROR     = 40, // things that go boom
WARNING   = 30, // things that may go boom later
INFO      = 20, // information of general interest
PROGRESS  = 16, // what's happening (broadly)
TRACE     = 13, // what's happening (in detail)
DBG       = 10  // sundry
"""
level = 16
set_log_level(level)

#To turn of logging completely, use : set_log_active(False)


tic = time.time()

# ------------------
# Parameters
# ------------------
ERROR = 40
set_log_level(ERROR) # log level
parameters.parse()   # read paramaters from command line
# set some dolfin specific parameters
parameters["form_compiler"]["optimize"]     = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "uflacs" # quadrature is deprecated

"""
# Parameter for solver

newton_Rtol       = 1e-6#1e-8 # relative tolerance for Newton solver (balance eq.)
newton_Atol       = 1e-6#1e-8 # absoulte tolerance for Newton solver (balance eq.)
newton_maxiter    = 20   # max. iteration for Newton solver (balance eq.)
snes_Rtol         = 1e-7#1e-9 # relative tolerance for SNES solver (phase field eq.)
snes_Atol         = 1e-7#1e-9 # absolute tolerance for SNES solver (phase field eq.)
snes_maxiter      = 30   # max. iteration for SNEs solver (phase field eq.)

"""


# ------------------
# folder name : save file Create VTK file for saving solution
# ------------------
#savedir        = "results_tract_lw_0_01_h0_01_SNES_sun/"
savedir        = "results_sh_lw_0_01_h0_0067_Amor_SNES_d_New_u_Hmod/"
vtkfileU       = File(savedir+'displacement.pvd')
vtkfileDamage  = File(savedir+'damage.pvd')
vtkfileSigma   = File(savedir+'stress.pvd')
vtkfileH       = File(savedir+'History_H.pvd')
vtkfilePsi_el  = File(savedir+'psi_elastic.pvd')


"""
=========================================================
                        Physical parameters
=========================================================
"""

E, nu     = Constant(210000), Constant(0.3) #[MPa]
G    = Constant(E / (2.0*(1.0 + nu))) # [MPa]
lmbda = Constant (E * nu / ((1.0 + nu)* (1.0 - 2.0*nu)))
K0, Gc    = Constant(lmbda+2./3.*G), Constant(2.7)
cw, lw    = Constant(2), Constant(0.01) # [mm]
psi_cr    = 0#Constant(Gc/(2*lw))



T = 0.02           # final time
num_steps = 200    # number of time steps
dt = T / num_steps # time step size
time_step = np.linspace(0, T, num_steps)
"""
=========================================================
                        Create mesh
=========================================================
"""

nx = 150
ny = 150 # 14 400 cell h = 0.01 mm
mesh = UnitSquareMesh(nx, ny,'crossed')
Nnode = len(mesh.coordinates())
Ncell = len(mesh.cells())
coord = mesh.coordinates()
ndim = mesh.topology().dim()



"""
=========================================================
                        Space discretization
=========================================================

"""

# V         = VectorFunctionSpace(mesh,'Lagrange', 1)
# V_alpha   = FunctionSpace(mesh,'Lagrange', 1)
# V_H       = FunctionSpace(mesh,'DG', 0)
# Tens      = TensorFunctionSpace(mesh, 'Lagrange', 1)

degU, degDam = 1, 1
degSig, degH = 0 , 0

V         = VectorFunctionSpace(mesh,'Lagrange', degU)
V_alpha   = FunctionSpace(mesh,'Lagrange', degDam)
V_H       = FunctionSpace(mesh,'DG', degH)
Tens      = TensorFunctionSpace(mesh, 'DG', degSig)

"""
=========================================================
                        Boundary condition
=========================================================
"""

Disp_shear      = Expression( ('t', '0.'), t = 0.0, degree = 1 ) # Constant((0.,0.))

# boolean boundary function
boundary_bottom  = 'near(x[1], 0)'
boundary_up      = 'near(x[1], 1)'
boundary_left    = 'near(x[0], 0)'
boundary_right   = 'near(x[0], 1)'
class Precrack(SubDomain):
    def inside(self, x, on_boundary):
        return (between(x[0], (0., 0.5))  and near(x[1], 0.5) ) # and : &&
crack  = Precrack()


# Initialize mesh function for exterior domains / corresponding tags to the boundaries
#Up, bottom, left, right and crack domain
bottom     = CompiledSubDomain(boundary_bottom)
up         = CompiledSubDomain(boundary_up)
left       = CompiledSubDomain(boundary_left)
right      = CompiledSubDomain(boundary_right)
boundaries = MeshFunction("size_t", mesh,1)
boundaries.set_all(0)
bottom.mark(boundaries, 1) # mark left as 1
up.mark(boundaries, 2) # mark right as 2
left.mark(boundaries, 3) # mark right as 3
right.mark(boundaries, 4) # mark right as 4
crack.mark(boundaries, 5) # mark right as 5
ds = Measure("ds",subdomain_data=boundaries) # left: ds(1), right: ds(2), crack : ds(4), ds(0) all the edges


bcu_bottom = DirichletBC( V , Constant((0.,0.)), boundary_bottom)
bcu_right  = DirichletBC( V.sub(1) , Constant(0.), boundary_right)
bcu_left   = DirichletBC( V.sub(1) , Constant(0.), boundary_left)
#bcu_up     = DirichletBC( V , Disp_tract, boundary_up )
bcu_up     = DirichletBC( V , Disp_shear, boundary_up )
bcu        = [bcu_bottom, bcu_up, bcu_right, bcu_left]
#bcu        = [bcu_bottom, bcu_up ]

bcp_bottom = DirichletBC(V_alpha, Constant(0.), boundary_bottom)
bcp_up     = DirichletBC(V_alpha, Constant(0.), boundary_up)
bcp_crack  = DirichletBC(V_alpha, Constant(1.), boundaries,5)
bc_alpha   = [bcp_bottom, bcp_up, bcp_crack]

"""
=========================================================
    Define Trial and Test Function
    Define functions for solutions at current times steps
=========================================================
"""
du,v,u_ = TrialFunction(V), TestFunction(V), Function(V)
dalpha,beta,alpha_ = TrialFunction(V_alpha), TestFunction(V_alpha), Function(V_alpha)



# INITIAL CONDITION
# u_n            = interpolate(Constant((0.,0.)),V)
# alpha_n        = interpolate(Constant(0.), V_alpha)
# H_n            = interpolate(Expression("0.0",degree=0), V_alpha)
# H_n_prev       = interpolate(Expression("0.0",degree=0), V_alpha)
u_old, alpha_old      = Function(V), Function(V_alpha)
H_new ,H_old          = Function(V_H),Function(V_H)
g              = gk(alpha_old,1.0e-6)
sig_funct      = Function(Tens, name="Stress")
Psi_elastic    = Function(V_H, name ="elastic_energy_density" )


"""
=========================================================
                        Define Variational problem
=========================================================
"""


#---------------- Mechanical equation
tn = Constant((0., 0.)) # external stress

#f_u = inner( sigma(u, g, G , lmbda, ndim) , epsilon(v) ) * dx + dot( tn, v ) * ds
f_u = inner( sigma_amor(u_, alpha_, G , K0, ndim) , epsilon(v) ) * dx + dot( tn, v ) * ds
a_u = lhs(f_u)
L_u = rhs(f_u)
J_u = derivative(f_u, u_, du) # jacobian

#--------------- Phase Field equation

#use trial function alpha and not solution function alpha_
f_alpha = H_new*inner(beta, gk_prime(alpha_)) * dx \
          + (Gc/(cw*lw)) * (beta*w_prime(alpha_) + (2.*lw**2)*inner(grad(beta), grad(alpha_))) * dx 
a_alpha = lhs(f_alpha)
L_alpha = rhs(f_alpha)
J_alpha = derivative(f_alpha, alpha_, dalpha) # jacobian
# Constraints for the phase field
alpha_min = interpolate(Constant(DOLFIN_EPS), V_alpha) # lower bound
alpha_max = interpolate(Constant(1.0), V_alpha)        # upper bound

# Solver parametrization

problem_u     = NonlinearVariationalProblem(f_u,u_,bcu , J_u)
problem_alpha = NonlinearVariationalProblem(f_alpha,alpha_ ,bc_alpha, J_alpha)
problem_alpha.set_bounds(alpha_min, alpha_max)
solver_u      = NonlinearVariationalSolver(problem_u)
solver_alpha  = NonlinearVariationalSolver(problem_alpha)

newton_prm = solver_u.parameters['newton_solver']
newton_prm['relative_tolerance'] = 1e-8
newton_prm['absolute_tolerance'] = 1e-8
newton_prm['maximum_iterations'] = 2000
newton_prm['error_on_nonconvergence'] = False

"""
snes_prm = {"nonlinear_solver": "snes",
            "snes_solver"     : { "method": "vinewtonssls",
                                  "line_search": "basic",
                                  "maximum_iterations": 2000,
                                  "relative_tolerance": 10e-8,
                                  "absolute_tolerance": 10e-8,
                                  "report": True,
                                  "error_on_nonconvergence": False,
                                }}
"""

snes_prm = {"nonlinear_solver": "snes",
            "snes_solver"     : { "method": "vinewtonssls",
                                  "maximum_iterations": 2000,
                                  "relative_tolerance": 1e-9,
                                  "absolute_tolerance": 1e-9,
                                  "report": True,
                                  "error_on_nonconvergence": False,
                                }}
solver_alpha.parameters.update(snes_prm)


# Stocked Forces and Energy value

forces        = np.zeros((len(time_step),3))
loading       = np.zeros((len(time_step),2))
energies      = np.zeros((len(time_step),3))
#Data_SIG      = np.zeros((num_steps, 3, Nnode))
Data_SIG      = np.zeros((num_steps, 3, Ncell))
Data_Usol     = np.zeros((num_steps, 2, Nnode))
H_tab         = np.zeros((num_steps, Ncell))


#---------------- RESOLUTION
t= 0.
it = 0
tol = 0.001 # 1e-6
maxiter = 300

stop_tt = 150
import ufl


for t in time_step[:stop_tt]:

    it +=1
    #Disp_tract.t = t
    #loading[it]= np.array([it,Disp_tract.t])
    Disp_shear.t = t
    loading[it]= np.array([it,Disp_shear.t])
    print("\n========================================================")
    print("\n -> Timestep : %d s, Time : %.3g s, Ux_given :  %.3g mm  "%(it, t, Disp_shear.t))
    print("\n========================================================")

    k = 0
    res_H = 1.

    print("\n ----- > Start Staggered Loop")
    while (res_H > tol) and (k < maxiter):

        k +=1
        #----Step 1 : Displacement resolution
        iter_u = solver_u.solve()


        #----Step 2 : Computation of H energy history
        #elastic_energy_density=project(0.5*inner(sigma_0(u_, G, lmbda,ndim), epsilon(u_)), V_alpha)
        elastic_energy_density=project(psi_plus(u_, K0,G), V_H)
#        H_new.vector().set_local(np.max((elastic_energy_density.vector().get_local(),H_new.vector().get_local()),axis=0))
        H_new.vector().set_local(np.max((elastic_energy_density.vector().get_local(),H_tab[it-1]),axis=0))
        print("\n________________________________________")
        print("\n PETscSNESSolver Parameters for Damage pb")
        print("__________________________________________")
        iter_alpha = solver_alpha.solve()
        print("\nEnd SNES Parameters__________________________")

        #---- Check Residual
        H_gap = H_new.vector().get_local() - H_old.vector().get_local()
        index_max = np.argmax(H_gap)
        H_gap_max = (H_new.vector().get_local() - H_old.vector().get_local()).max()
        res_H = abs(H_gap_max)/ H_old.vector().get_local()[index_max]
        
        err_u      = errornorm(u_, u_old, norm_type = 'l2', mesh = None)
        err_alpha  = errornorm(alpha_, alpha_old,   norm_type = 'l2', mesh = None)
        err = max(err_u, err_alpha) 

        
        #---- Step 4 : Update previous solution
        u_old.assign(u_)
        alpha_old.assign(alpha_)
        g = gk(alpha_old,1.0e-6)
        H_old.assign(H_new)

        print( "\nIteration: %d, err_u: %.8g, err_alpha : %.8g, Residual: %.8g \n" % (k, err_u, err_alpha, res_H))
        
 

    
    iter = [k]
    print("\n ----- > Timestep : %d"%(it))
    #print('type u_', type(u_))
    ux = u_.sub(0, deepcopy=True)
    uy = u_.sub(1, deepcopy=True)
    #print(ux.vector()[:])
    H_tab[it, :] = project(H_new, V_H).vector().get_local()
    print(' H_tab[it,:] : ',  H_tab[it, :])
    Psi_elastic.assign(project(psi(u_,alpha_, G, K0), V_H))

    # vertice value (mapping different from ddf value)
    Data_Usol[it, 0, :] = ux.compute_vertex_values()[:]
    Data_Usol[it, 1, :] = uy.compute_vertex_values()[:]

    
    sig_funct.assign(project(sigma_amor(u_, alpha_, G , K0, ndim), Tens))
    s11 = project(sigma_amor(u_, alpha_, G , K0, ndim)[0, 0], V_H)
    Data_SIG[it, 0, :] = s11.vector().get_local()
    s22 = project(sigma_amor(u_, alpha_, G , K0, ndim)[1, 1], V_H)
    Data_SIG[it, 1, :] = s22.vector().get_local()
    s12 = project(sigma_amor(u_, alpha_, G , K0, ndim)[1, 0], V_H)
    Data_SIG[it, 2, :] = s12.vector().get_local()
        
    # Calculate the axial and tangent force resultant
    Fn = assemble(sigma_amor(u_,alpha_, G , K0, ndim)[0,0]*ds(2))
    Ft = assemble(sigma_amor(u_,alpha_, G , K0, ndim)[1,1]*ds(2))
    forces[it] = np.array([t,Fn, Ft])
    #damage_data[it,:] = np.array(alpha_.vector())

    
    #Calculate individually the energy
    elastic_energy       = 0.5*inner(sigma_amor(u_,alpha_, G , K0, ndim), epsilon(u_))*dx
    dissipated_energy    = Gc/cw*(w(alpha_)/lw + lw*dot(grad(alpha_), grad(alpha_)))*dx
    elastic_energy_value = assemble(elastic_energy)
    surface_energy_value = assemble(dissipated_energy)

    if it == 1 :
        surf_off_set = surface_energy_value
        print(' surf_offset_precrack : ',  surf_off_set)
        
    energies[it]  = np.array([t,elastic_energy_value,surface_energy_value - surf_off_set])
    

    # monitor the results    


    if MPI.comm_world.rank == 0:
        print( "Eel_max: %.8g \nH_n_max: %.8g \nalpha_max: %.8g" % (elastic_energy_density.vector().max(), H_new.vector().max(), alpha_.vector().max()) )
        print("\nIterationU and IterationAlpha: (%i,%i)"%(iter_u[0],iter_alpha[0]))
        
        print("\nElastic and surface energies : (%g,%g)"%(elastic_energy_value,surface_energy_value - surf_off_set)) # WHY they display a scalar (a value) and not scale field (vector)
        print("-------------------------------------------------------------")

    #---- Save in VTK file
    vtkfileU      << u_
    vtkfileDamage << alpha_
    vtkfileSigma  << sig_funct
    vtkfileH      << H_new
    vtkfilePsi_el << Psi_elastic

    #---Save Data in txt file

    np.savetxt(savedir + 'forces.txt'  , forces)
    np.savetxt(savedir + 'loading.txt'  , loading)
    np.savetxt(savedir + 'energies.txt', energies)
    np.savetxt(savedir + 'sigma11.txt' , Data_SIG[:, 0, :])  # time first, 0 : component, node third
    np.savetxt(savedir + 'uxsolve.txt'  , Data_Usol[:, 0, :]) # ux for all node
    np.savetxt(savedir + 'uysolve.txt'  , Data_Usol[:, 1, :]) # ux for all node



#Evaluate TIME Processing 

toc = time.time() - tic

print('Elapsed CPU time: ', toc, '[sec]')


#POSTPROCESSING

"""
Data_sig11     = np.loadtxt(savedir+'sigma11.txt')
Data_ux        = np.loadtxt(savedir+'uxsolve.txt')
Data_uy        = np.loadtxt(savedir+'uysolve.txt')
Data_energies  = np.loadtxt(savedir+'energies.txt')
Data_forces    = np.loadtxt(savedir+'forces.txt')
Data_load      = np.loadtxt(savedir+'loading.txt')


plt.figure(1)
plt.plot(Data_load[:,1], -Data_forces[:,1], 'b*', linewidth = 2, label = '|Fn|')
plt.plot(Data_load[:,1], -Data_forces[:,2], 'r*', linewidth = 2, label = '|Ft|')
plt.legend()
plt.xlabel('Displacement [mm]')
plt.ylabel('Force [N]')

plt.figure(2)
plt.plot(Data_load[:,1], Data_energies[:,1], 'b*', linewidth = 2, label = 'elastic_energy')
plt.plot(Data_load[:,1], Data_energies[:,2], 'r*', linewidth = 2, label = 'dissipated energy')
plt.xlabel('Displacement [mm]')
plt.ylabel('energy [J]')
plt.legend()

plt.figure(3)
plt.plot(Data_load[:,0], Data_load[:,1], 'g*', linewidth = 2, label = 'Disp_load')
plt.plot(Data_load[:,0], Data_uy[:,50], 'b*', linewidth = 2, label = 'uy vertice 50')
plt.plot(Data_load[:,0], Data_uy[:,2], 'r*', linewidth = 2, label = 'uy vertice 2')
plt.plot(Data_load[:,0], Data_uy[:,20000], 'k*', linewidth = 2, label = 'uy vertice 20000')
plt.xlabel('time [s]')
plt.ylabel('Disp [mm]')
plt.legend()        


plt.show()

"""



    
