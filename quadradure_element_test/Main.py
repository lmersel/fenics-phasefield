import fenics as fe
import numpy as np
from Function import *
import matplotlib.pyplot as plt

import sys
import time

"""
   |-------------------------------|
   |Single Notch Edge Shear Test   |
   |-------------------------------|
"""

fe.set_log_level(50)
fe.parameters["form_compiler"].update(
{"optimize": True,
"cpp_optimize": True,
"representation":"uflacs",
"log_level": 25,
"split": True,
"quadrature_degree": 4})

tic = time.time()

# ------------------
# Parameters
# ------------------
# ERROR = 40
# set_log_level(ERROR) # log level
# parameters.parse()   # read paramaters from command line
# parameters["form_compiler"]["optimize"]     = True
# parameters["form_compiler"]["cpp_optimize"] = True
# parameters["form_compiler"]["representation"] = "uflacs" # quadrature is deprecated



# ------------------
# folder name : save file Create VTK file for saving solution
# ------------------
#savedir        = "results_tract_lw_0_01_h0_01_SNES_sun/"
savedir        = "results_sh_lw_0_01_h0_0067_Hmod_QuadSp/"
vtkfileU       = fe.File(savedir+'displacement.pvd')
vtkfileSigma   = fe.File(savedir+'stress.pvd')

"""
=========================================================
                        Physical parameters
=========================================================
"""

E, nu     = fe.Constant(210000), fe.Constant(0.3) #[MPa]
G         = fe.Constant(E / (2.0*(1.0 + nu))) # [MPa]
lmbda     = fe.Constant (E * nu / ((1.0 + nu)* (1.0 - 2.0*nu)))
K0, Gc    = fe.Constant(lmbda+2./3.*G), fe.Constant(2.7)
cw, lw    = fe.Constant(2), fe.Constant(0.01) # [mm]

T, num_steps = 0.02 , 200      
time_step = np.linspace(0, T, num_steps)
"""
=========================================================
                        Create mesh
=========================================================
"""

nx = 150
ny = 150 # 14 400 cell h = 0.01 mm
mesh = fe.UnitSquareMesh(nx, ny,'crossed')
Nnode = len(mesh.coordinates())
Ncell = len(mesh.cells())
coord = mesh.coordinates()
ndim = mesh.topology().dim()

"""
=========================================================
                        Space discretization
=========================================================

Degree of the Quadrature element 
-  degree=1 yields only 1 Gauss point, degree=2 yields 3 Gauss points and degree=3 yields 6 Gauss points
"""
degU ,degDam       = 2, 2

deg_history, deg_stress = 2, 2

V           = fe.VectorFunctionSpace(mesh,'Lagrange', degU)
V_alpha     = fe.FunctionSpace(mesh,'Lagrange', degDam)



W0e         = fe.FiniteElement("Quadrature", mesh.ufl_cell(), degree=deg_history, quad_scheme='default')
V_H         = fe.FunctionSpace(mesh, W0e)

WWe         = fe.TensorElement("Quadrature", mesh.ufl_cell(), degree=deg_stress, shape=(3,3), quad_scheme='default')
Tens        = fe.FunctionSpace(mesh, WWe)

We          = fe.VectorElement("Quadrature", mesh.ufl_cell(), degree=deg_stress, dim=4, quad_scheme='default')
Vect        = fe.FunctionSpace(mesh, We)

Vsig_DG     = fe.TensorFunctionSpace(mesh, "DG", degree=0)
Vvec_DG     = fe.VectorFunctionSpace(mesh, "DG", degree=0)
#V_H         = fe.FunctionSpace(mesh,'DG', degree=0)

"""
=========================================================
                        Boundary condition
=========================================================
"""
Disp_shear      = fe.Expression( ('t', '0.'), t = 0.0, degree = 1 ) # Constant((0.,0.))

# boolean boundary function
boundary_bottom  = 'near(x[1], 0)'
boundary_up      = 'near(x[1], 1)'
boundary_left    = 'near(x[0], 0)'
boundary_right   = 'near(x[0], 1)'

bottom     = fe.CompiledSubDomain(boundary_bottom)
up         = fe.CompiledSubDomain(boundary_up)
left       = fe.CompiledSubDomain(boundary_left)
right      = fe.CompiledSubDomain(boundary_right)
boundaries = fe.MeshFunction("size_t", mesh,1)
boundaries.set_all(0)
bottom.mark(boundaries, 1) # mark left as 1
up.mark(boundaries, 2) # mark right as 2
left.mark(boundaries, 3) # mark right as 3
right.mark(boundaries, 4) # mark right as 4
ds = fe.Measure("ds",subdomain_data=boundaries) # left: ds(1), right: ds(2), crack : ds(4), ds(0) all the edges


bcu_bottom = fe.DirichletBC( V , fe.Constant((0.,0.)), boundary_bottom)
bcu_right  = fe.DirichletBC( V.sub(1) , fe.Constant(0.), boundary_right)
bcu_left   = fe.DirichletBC( V.sub(1) , fe.Constant(0.), boundary_left)
bcu_up     = fe.DirichletBC( V , Disp_shear, boundary_up )
bcu        = [bcu_bottom, bcu_up, bcu_right, bcu_left]


"""
=========================================================
    Define Trial and Test Function
    Define functions for solutions at current times steps
=========================================================
"""
u_trial ,u_test         = fe.TrialFunction(V), fe.TestFunction(V)
u , u_old     = fe.Function(V), fe.Function(V)

du = fe.Function(V, name="Iteration correction")
Du = fe.Function(V, name="Current increment")

sig    = fe.Function(Vect, name="Stress")
#sigma_projected = Function(Vsig_DG , name="Stress")
sigma_projected = fe.Function(Vvec_DG , name="Stress") # for the postprocessing


def local_project(v, V, u=None):
    """
    projects v on V with custom quadrature scheme dedicated to
    FunctionSpaces V of `Quadrature` type

    if u is provided, result is appended to u
    """
    dv = TrialFunction(V)
    v_ = TestFunction(V)
    a_proj = inner(dv, v_)*dxm
    b_proj = inner(v, v_)*dxm
    solver = LocalSolver(a_proj, b_proj)
    solver.factorize()
    if u is None:
        u = Function(V)
        solver.solve_local_rhs(u)
        return u
    else:
        solver.solve_local_rhs(u)
        return

    
"""
=========================================================
                        Define Variational problem
=========================================================
"""
metadata = {"quadrature_degree": deg_stress, "quadrature_scheme": "default"}
dxm = fe.dx(metadata=metadata)

#---------------- Mechanical equation
tn = fe.Constant((0., 0.)) # external stress


## Usual method : derivature not work with Quadrature element ===> # Segmentation fault
#f_u = fe.inner( as_3D_tensor(sigma_amor(u, G , K0, ndim)), epsilon(u_) ) * dxm + fe.dot( tn, u_ ) * ds
#J_u = fe.derivative(f_u, u, v) 
#problem_u     = fe.NonlinearVariationalProblem(f_u,u,bcu , J_u)


# 2nd method with Linear solver: J_u * du = f_u ==> u = u +du (an iteration of the Newton Solver)
# Problem with ufl.Abs() nonlinear operator
f_u = fe.inner( as_3D_tensor(sig), epsilon(u_test ) ) * dxm + fe.dot( tn, u_test  ) * ds
J_u = fe.inner(as_3D_tensor(sigma_amor(u_test , G , K0, ndim)), epsilon(u_trial))*dxm # jacobian

# # NL solver F_u == 0 (J_u * du - f_u = 0 ) ===> # Segmentation fault
# F_u       = fe.inner(as_3D_tensor(sigma_amor(du, G , K0, ndim)), epsilon(u_test))*dxm +\
#             fe.inner( as_3D_tensor(sig), epsilon(u_test) ) * dxm + fe.dot( tn, u_test ) * ds
# H_u       = fe.derivative(F_u, du, u_trial) 
# problem_u = fe.NonlinearVariationalProblem(F_u, du, bcu , H_u)

#f_u = inner(sig, as_2D_vector(epsilon(u_)) ) * dxm + dot( tn, u_ ) * ds
#J_u = inner(sigma_amor(u_, G , K0, ndim), as_2D_vector(epsilon(v)))*dxm # jacobian




# solver_u      = fe.NonlinearVariationalSolver(problem_u)
# snes_prm = {"nonlinear_solver": "snes",
#             "snes_solver"     : { "method": "newtons",
#                                   "maximum_iterations": 2000,
#                                   "relative_tolerance": 1e-9,
#                                   "absolute_tolerance": 1e-9,
#                                   "report": True,
#                                   "error_on_nonconvergence": False,
#                                 }}
# solver_u.parameters.update(snes_prm)

#---------------- RESOLUTION
t= 0.
it = 0
tol = 0.001 # 1e-6
maxiter = 300

stop_tt = 5
import ufl


for t in time_step[:stop_tt]:

    it +=1
    Disp_shear.t = t
    print("\n========================================================")
    print("\n -> Timestep : %d s, Time : %.3g s, Ux_given :  %.3g mm  "%(it, t, Disp_shear.t))
    print("\n========================================================")

    k = 0
    err_u = 1.
    A, Res = fe.assemble_system(J_u, f_u, bcu)
    Du.interpolate(fe.Constant((0, 0)))

    
    print("\n ----- > Start Staggered Loop")
    while (err_u > tol) and (k < maxiter):

        k +=1
        #----Step 1 : Displacement resolution

        
        
        ## Method 1  : L solver FAILED
        fe.solve(A, du.vector(), Res, "cg")
        Du.assign(Du+du)
        u.assign(u+Du)

        ## Method 2  : NL solver FAILED
        #fe.solve(F_u == 0.0, du,bcu,\
        #         solver_parameters = { "newton_solver":{"relative_tolerance":1e-6} }) # Newton Raphson
        #solver_u.solve() # PetscSNES

        ## Method 3  : L solver FAILED
        #fe.solve(J_u == f_u, du,bcu)

        
        sig_ = sigma_amor(u, G, K0,ndim,V_H)
        #print('type(sig_)',type(sig_))
        #sigma_projected.assign(project(sigma_amor(u, G, K0,ndim), Vsig_DG))
        #sigma_projected.assign(project(sigma_amor(u, G, K0,ndim), Tens, form_compiler_parameters={"quadrature_degree":deg_stress}))
        

        #local_project(v, V)
        err_u      = fe.errornorm(u, u_old, norm_type = 'l2', mesh = None)
        print('iteration : %i, err_u : %0.8g'% (k,err_u))
        
        #---- Step 4 : Update previous solution
        u_old.assign(u)

        #---- Step 5 : Projection on Quadrature Space
        local_project(sig_, Vect,sig)
        A, Res = assemble_system(J_u, f_u, bcu)
    
    iter = [k]
    
    # Calculate the axial and tangent force resultant
    #Fn = fe.assemble(sigma_amor(u, G , K0, ndim)[0]*ds(2))
    #Ft = fe.assemble(sigma_amor(u,G , K0, ndim)[1]*ds(2))

    #Calculate individually the energy
    #elastic_energy       = 0.5*fe.inner(as_3D_tensor(sigma_amor(u, G , K0, ndim)), epsilon(u))*dxm
    #elastic_energy_value = fe.assemble(elastic_energy)


    #print("\nElastic  energies : %g "%(elastic_energy_value)) # WHY they display a scalar (a value) and not scale field (vector)
    print("-------------------------------------------------------------")

    #---- Save in VTK file
    vtkfileU      << u
    #vtkfileSigma  << sigma_projected


    
