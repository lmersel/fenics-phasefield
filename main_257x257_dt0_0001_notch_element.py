import ufl
import matplotlib.pyplot as plt
import numpy as np
from dolfinx import log, io, mesh, fem, cpp, plot
from mpi4py import MPI
from petsc4py import PETSc
import meshio

log.set_log_level(log.LogLevel.OFF)


savedir        = "RESULT_257x257_notch_cells_TEST/"


comm = MPI.COMM_WORLD
msh = mesh.create_unit_square(comm , 257, 257, mesh.CellType.quadrilateral)


ndim = msh.topology.dim
fdim = ndim - 1
num_cells = msh.topology.index_map(ndim).size_local
points = msh.geometry.x
Nnode,Ncell = len(points),  num_cells

h_min = cpp.mesh.h(msh, ndim, range(num_cells))[0]
print("h_min", h_min)
lw= 3*h_min


# Material Parameters
E, nu     = PETSc.ScalarType(210000), PETSc.ScalarType(0.3) #[MPa]
G         = fem.Constant(msh,E / (2.0*(1.0 + nu))) # [MPa]
lmbda     = fem.Constant(msh,E * nu / ((1.0 + nu)* (1.0 - 2.0*nu)))
K0, Gc    = lmbda+2/3*G, fem.Constant(msh, PETSc.ScalarType(2.7))
cw        = fem.Constant(msh, PETSc.ScalarType(2)) # [mm]



# Time discretization
t = 0.0
T = 0.013                            # final time
num_steps = 130                     # number of time steps
time_step = np.linspace(0, T, num_steps)


# Space discretization

degU, degDam = 1, 1
degSig, degH = 0 , 0

FE_vector=ufl.VectorElement("CG",msh.ufl_cell(),1)
V_u=fem.FunctionSpace(msh,FE_vector)
FE_scalar=ufl.FiniteElement("CG",msh.ufl_cell(),1)
V_alpha=fem.FunctionSpace(msh,FE_scalar)
V_H       = fem.FunctionSpace(msh,('DG', degH))
Tens      = fem.TensorFunctionSpace(msh, ('DG', degSig))
print(np.shape(msh.geometry.x))

# BOUNDARY CONDTION

def notch(x):
    return np.logical_and(np.isclose(x[1], 0.5), x[0] < 0.5)

def notch_cell(x):
    return np.logical_and( np.logical_and(0.498 < x[1], x[1] < 0.502) , x[0] < 0.5)

facet_top     = mesh.locate_entities(msh, fdim, lambda x: np.isclose(x[1], 1))
facet_bottom  = mesh.locate_entities(msh, fdim, lambda x: np.isclose(x[1], 0))
facet_right   = mesh.locate_entities(msh, fdim, lambda x: np.isclose(x[0], 1))
facet_left    = mesh.locate_entities(msh, fdim, lambda x: np.isclose(x[0], 0))
facet_notch   = mesh.locate_entities(msh, fdim, notch_cell)

facet_indices, facet_markers = [], []
facet_indices.append(facet_top)
facet_indices.append(facet_bottom)
facet_indices.append(facet_right)
facet_indices.append(facet_left)
facet_indices.append(facet_notch)

facet_markers.append(np.full(len(facet_top ), 1))
facet_markers.append(np.full(len(facet_bottom ), 2))
facet_markers.append(np.full(len(facet_right ), 3))
facet_markers.append(np.full(len(facet_left ), 4))
facet_markers.append(np.full(len(facet_notch ), 5))


# create MeshTags identifying the facets for each boundary condition
facet_indices = np.array(np.hstack(facet_indices), dtype=np.int32)
facet_markers = np.array(np.hstack(facet_markers), dtype=np.int32)
sorted_facets = np.argsort(facet_indices)
facet_tag = mesh.meshtags(msh, fdim, facet_indices[sorted_facets], facet_markers[sorted_facets])

facet_left_dofs = fem.locate_dofs_topological(V_u.sub(1), fdim, facet_left)
facet_right_dofs = fem.locate_dofs_topological(V_u.sub(1), fdim, facet_right)
facet_bottom_dofs = fem.locate_dofs_topological(V_u,fdim, facet_bottom)
#facet_bottom_dofs = fem.locate_dofs_geometrical(V_u,lambda x: np.isclose(x[1], 0))
facet_top_dofs = fem.locate_dofs_topological(V_u, fdim, facet_top)
facet_notch_dofs = fem.locate_dofs_topological(V_alpha, fdim, facet_notch)


# displacement
u_clamped_xy = np.array((0. ,)*msh.geometry.dim, dtype=PETSc.ScalarType) # print(u_zeros) = [0.0, 0.0]

class TopLoad():
    def __init__(self,t):
        self.t = t

    def __call__(self,x):
        values = np.zeros((ndim, x.shape[1]),dtype=PETSc.ScalarType)
        values[0] = self.t
        return values
u_app_func = fem.Function(V_u)
u_app = TopLoad(t)
u_app_func.interpolate(u_app)


bc_left = fem.dirichletbc( fem.Constant(msh, PETSc.ScalarType(0.)), facet_left_dofs,V_u.sub(1))
bc_right = fem.dirichletbc( fem.Constant(msh, PETSc.ScalarType(0.)), facet_right_dofs,V_u.sub(1))
bc_bottom = fem.dirichletbc( u_clamped_xy,facet_bottom_dofs, V_u)
bc_top = fem.dirichletbc( u_app_func, facet_top_dofs)
bcu = [bc_top, bc_right, bc_bottom,bc_left ]

#Build homogeneous bc for displacement
u_app_func_h = fem.Function(V_u)
u_app_0 = TopLoad(t=0.)
u_app_func_h.interpolate(u_app_0)
bc_top_h = fem.dirichletbc( u_app_func_h, facet_top_dofs)
bcu_h = [bc_top_h, bc_right, bc_bottom,bc_left ]


bcalpha = [fem.dirichletbc( fem.Constant(msh, PETSc.ScalarType(1.)), facet_notch_dofs,V_alpha)]

#Build homogeneous bc for damage
bcalpha_h = [fem.dirichletbc( fem.Constant(msh, PETSc.ScalarType(0.)), facet_notch_dofs,V_alpha)]


# specify the minimun quadrature degree required in function of the integration (stiffness, Jacobian matrix)
metadata = {"quadrature_degree": 4}
ds = ufl.Measure("ds", domain=msh, subdomain_data=facet_tag, metadata=metadata)
dx = ufl.Measure("dx", domain=msh, metadata=metadata)




# Function util
def w(alpha):
    return alpha*alpha

def w_prime(alpha):
    return 2*alpha #1.0

#Degradation function
def gk(alpha, k= 1e-6):
    return (1-alpha)**2 + k


def gk_prime(alpha, k= 1e-6):
    return 2*(alpha-1)

def epsilon(u):
    return ufl.sym(ufl.nabla_grad(u))


def psi_plus(u, K0,G):
    eps = epsilon(u)
    plus_tr_eps = 0.5*(ufl.tr(eps)+abs(ufl.tr(eps)))
    return 0.5* K0*(plus_tr_eps)**2 + G *ufl.inner(ufl.dev(eps),ufl.dev(eps))

def psi_moins(u, K0,G):
    eps = epsilon(u)
    moins_tr_eps = 0.5*(ufl.tr(eps)-abs(ufl.tr(eps)))
    return 0.5* K0*(moins_tr_eps)**2

def psi_0(u, G, K0):
    return  psi_plus(u, K0,G)+psi_moins(u, K0,G)

def psi(u,alpha, G, K0):
    return  gk(alpha, k= 1e-6)*psi_plus(u, K0,G)+psi_moins(u, K0,G)


def sigma_amor(u, alpha , G, K0,ndim):
    eps = epsilon(u)
    plus_tr_eps     = 0.5*(ufl.tr(eps)+abs(ufl.tr(eps)))
    moins_tr_eps    = 0.5*(ufl.tr(eps)-abs(ufl.tr(eps)))
    psi_plus_prime  = 0.5* K0 * plus_tr_eps * ufl.Identity(ndim) + 2*G *ufl.dev(eps)
    psi_moins_prime = 0.5* K0 * moins_tr_eps * ufl.Identity(ndim)
    return  gk(alpha, k= 1e-6) * psi_plus_prime + psi_moins_prime



def newton_solver(u, res, bcs,bcs_hom , jac, max_iter=100, rtol=1e-10, atol=1e-6 ):
    #applied BC + homogenize BCs
    fem.petsc.set_bc(u.vector, bcs)
    bcs = bcs_hom

    jac_form, res_form = fem.form(jac), fem.form(-res)
    J = fem.petsc.assemble_matrix(jac_form, bcs = bcs)
    J.assemble()
    b = fem.petsc.create_vector(res_form)
    delta_u = J.createVecRight()

    for k in range(max_iter):

        # Assemble the system for the Newton update with boundary conditions

        with b.localForm() as loc_1:
            loc_1.set(0)
        b = fem.petsc.assemble_vector(res_form)
        fem.petsc.apply_lifting(b, [jac_form], [bcs]) 
        b.ghostUpdate(addv=PETSc.InsertMode.ADD_VALUES, mode=PETSc.ScatterMode.REVERSE)
        fem.petsc.set_bc(b, bcs)
        
        # Create CG Krylov solver and turn convergence monitoring on
        solver = PETSc.KSP().create(MPI.COMM_WORLD)
        pc = solver.getPC()
        solver.setType('preonly')
        pc.setType('lu')
        solver.setConvergenceHistory()

        # Set matrix operator
        solver.setOperators(J)

        # Compute solution
        # residual compute by petsc with SetMonitor
        solver.setMonitor(lambda ksp, its, rnorm: print("Iteration: {}, rel. residual: {}".format(its, rnorm))) 
        solver.solve(b, delta_u)
        

        # residual compute in direct way
        residual = J * delta_u - b
        
        print("|b| : ", b.norm())
        print(f"|J*du-b|/|b|: {residual.norm() / b.norm()}.") # |Ax_b|/|b|
        

        # Update the solution
        u.x.array[:] = u.x.array[:] + delta_u.array[:]

        

        if   residual.norm() /b.norm() < rtol and k < max_iter:
            break


    else:
        raise RuntimeError("could not converge, norm_u {}, norm_delta_u {}".format(norm_u, norm_delta_u))


    return k

"""
=========================================================
    Define Trial and Test Function
    Define functions for solutions at current times steps
=========================================================
"""

alpha_trial,beta,alpha= ufl.TrialFunction(V_alpha), ufl.TestFunction(V_alpha), fem.Function(V_alpha)
alpha_old             =fem.Function(V_alpha)
alpha.name = 'Damage'
alpha_old.name        ="previous_iteration_damage"

u_trial,v,u           = ufl.TrialFunction(V_u), ufl.TestFunction(V_u), fem.Function(V_u)
u_old                 = fem.Function(V_u)
u.name = 'Displacement'
u_old.name            = "previous_iteration_displacement"


H_new ,H_old          = fem.Function(V_H),fem.Function(V_H)
H_inc                 = fem.Function(V_H)
H_new.name            ="current_iteration_history"
H_old.name            ="previous_iteration_history"
H_inc.name            ="previous_increment_history"
sig_funct             = fem.Function(Tens)
sig_funct.name        ="stress"
eps_funct             = fem.Function(Tens)
eps_funct.name        ="strain"


# s10_funct             = fem.Function(V_H)
# s10_funct.name        ="s10"
# e10_funct             = fem.Function(V_H)
# e10_funct.name        ="e10"


# INITIAL CONDITION

def u_zeros_init(x):
    values = np.zeros((2, x.shape[1]))
    values[0] = 0.0
    values[1] = 0.0
    return values
u_old.interpolate(u_zeros_init)
def alpha_zeros_init(x):
    values = np.zeros((1, x.shape[1]))
    return values
alpha_old.interpolate(alpha_zeros_init)

Pel_pos_expr          = fem.Expression(psi_plus(u,K0,G), V_H.element.interpolation_points)
H_new.interpolate(Pel_pos_expr) # Initialisation


sig_expr              = fem.Expression(sigma_amor(u, alpha, G , K0, ndim), Tens.element.interpolation_points)
sig_funct.interpolate(sig_expr) # Initialisation

eps_expr              = fem.Expression(epsilon(u), Tens.element.interpolation_points)
eps_funct.interpolate(eps_expr) # Initialisation



# s10_expr              = fem.Expression(sigma_amor(u, alpha, G , K0, ndim)[1,0], V_H.element.interpolation_points)
# s10_funct.interpolate(s10_expr) # Initialisation

# e10_expr              = fem.Expression(epsilon(u)[1,0], V_H.element.interpolation_points)
# e10_funct.interpolate(e10_expr) # Initialisation
"""
=========================================================
                        Define Variational problem
=========================================================
"""


Psi_elastic           = fem.Function(V_H, name ="elastic_energy_density" )
psi_elastic_pos       = fem.Function(V_H, name ="positive elastic energy density " )
g                     = gk(alpha_old,1.0e-6)



#---------------- Mechanical equation
residual = ufl.inner( sigma_amor(u, alpha, G , K0, ndim) , epsilon(v) ) * dx# - dot( tn, v ) * ds #residual
jacobian = ufl.derivative(residual, u, u_trial) # jacobian

#--------------- Phase Field equation
f_alpha = H_new*ufl.inner(beta, gk_prime(alpha)) * dx \
          + (Gc/(cw*lw)) * (beta*w_prime(alpha) + (2.*lw**2)*ufl.inner(ufl.grad(beta), ufl.grad(alpha))) * dx 
J_alpha = ufl.derivative(f_alpha, alpha, alpha_trial) # jacobian



# Stocked Forces and Energy value

forces        = np.zeros((len(time_step),3))
loading       = np.zeros((len(time_step),2))
energies      = np.zeros((len(time_step),3))
React         = np.zeros((len(time_step),3))


with io.XDMFFile(msh.comm, savedir+"displacement.xdmf", "w") as xdmf0:
    xdmf0.write_mesh(msh)
    xdmf0.write_function(u, t)

with io.XDMFFile(msh.comm, savedir+"damage.xdmf", "w") as xdmf1:
    xdmf1.write_mesh(msh)
    xdmf1.write_function(alpha, t)

with io.XDMFFile(msh.comm, savedir+"stress.xdmf", "w") as xdmf3:
    xdmf3.write_mesh(msh)
    xdmf3.write_function(sig_funct, t)

with io.XDMFFile(msh.comm, savedir+"strain.xdmf", "w") as xdmf4:
    xdmf4.write_mesh(msh)
    xdmf4.write_function(eps_funct, t)
    
# with io.VTKFile(msh.comm, savedir+"displacement.pvd", "w") as vtk0:
#     vtk0.write_mesh(msh)
#     vtk0.write_function(u,t)

# with io.VTKFile(msh.comm, savedir+"damage.pvd", "w") as vtk1:
#     vtk1.write_mesh(msh)
#     vtk1.write_function(alpha,t)

# with io.VTKFile(msh.comm, savedir+"s10.pvd", "w") as vtk4:
#     vtk4.write_mesh(msh)
#     vtk4.write_function(s10_funct,t)

# with io.VTKFile(msh.comm, savedir+"e10.pvd", "w") as vtk5:
#     vtk5.write_mesh(msh)
#     vtk5.write_function(e10_funct,t)

    


   

  

#---------------- RESOLUTION
t= 0.
#it_time = 0
tol = 0.001 # 1e-6
maxiter = 300
begin_tt = 1
it_time = begin_tt

for t in time_step[begin_tt:4]:
    
    u_app.t = t
    u_app_func.interpolate(u_app)
    loading[it_time] = u_app.t
    print("\n================================================================")
    print("\n -> Time increment: %d, Time: %.3g s, Ux_given: %.3g mm  "%(it_time, t, u_app.t))
    print("\n================================================================")

    it_staggered = 0
    res_H = 1.
    err = 1.
    print("\n ----- > Start Staggered Loop")
    while (res_H > tol) and (it_staggered < maxiter):
        it_staggered +=1

        #----Step 1 : Displacement resolution
        print("\n NRSolver for the displacement pb ")
        print("_____________________________________")

        bc_left = fem.dirichletbc( fem.Constant(msh, PETSc.ScalarType(0.)), facet_left_dofs,V_u.sub(1))
        bc_right = fem.dirichletbc( fem.Constant(msh, PETSc.ScalarType(0.)), facet_right_dofs,V_u.sub(1))
        bc_bottom = fem.dirichletbc( u_clamped_xy,facet_bottom_dofs, V_u)
        bc_top = fem.dirichletbc( u_app_func, facet_top_dofs)
        bcu = [bc_top, bc_right, bc_bottom,bc_left ]

        #Build homogeneous bc
        u_app_0 = TopLoad(t=0.)
        u_app_func_h.interpolate(u_app_0)
        bc_top_h = fem.dirichletbc( u_app_func_h, facet_top_dofs)
        bcu_h = [bc_top_h, bc_right, bc_bottom,bc_left]

        #Newton Raphson resolution
        iter_u = newton_solver( u,  res = residual, bcs = bcu,bcs_hom = bcu_h, jac = jacobian)

        #----Step 2 : Computation of H energy history

        Pel_pos_expr = fem.Expression(psi_plus(u,K0,G), V_H.element.interpolation_points)
        psi_elastic_pos.interpolate(Pel_pos_expr)
        H_new.x.array[:] = np.max((psi_elastic_pos.x.array[:], H_inc.x.array[:]),axis=0)


        #----Step 3 : Damge resolution

        print("\n NRSolver for damage pb")
        print("_____________________________________")
        
        bcalpha = [fem.dirichletbc( fem.Constant(msh, PETSc.ScalarType(1.)), facet_notch_dofs,V_alpha)]

        #Build homogeneous bc
        bcalpha_h = [fem.dirichletbc( fem.Constant(msh, PETSc.ScalarType(0.)), facet_notch_dofs,V_alpha)]

        #Newton Raphson resolution
        iter_alpha = newton_solver( alpha,  res = f_alpha, bcs = bcalpha,bcs_hom = bcalpha_h, jac =  J_alpha)
        

        print("_____________________________________")

        H_gap = H_new.vector.array - H_old.vector.array
        index_max,H_gap_max = np.argmax(H_gap), H_gap.max()
        res_H = abs(H_gap_max)/ H_old.vector.array[index_max]
        
        print('(Stag. Iteration : %g ,rel. residual : %g)'%(it_staggered,res_H))
        

        #---- Step 4 : Update previous solution
        u_old.x.array[:] = u.x.array[:]
        alpha_old.x.array[:] = alpha.x.array[:]
        g = gk(alpha_old,1.0e-6)
        H_old.x.array[:] = H_new.x.array[:]
        

    print("\n ----- > END Staggered Loop")
    H_inc.x.array[:] = H_new.x.array[:]

    sig_expr              = fem.Expression(sigma_amor(u, alpha, G , K0, ndim), Tens.element.interpolation_points)
    sig_funct.interpolate(sig_expr) 

    # s10_expr              = fem.Expression(sigma_amor(u, alpha, G , K0, ndim)[1,0], V_H.element.interpolation_points)
    # s10_funct.interpolate(s10_expr) 
    # e10_expr              = fem.Expression(epsilon(u)[1,0], V_H.element.interpolation_points)
    # e10_funct.interpolate(e10_expr) 



    
    #Calculate the axial and tangent force resultant
    Fx = fem.assemble_scalar(fem.form(sigma_amor(u,alpha, G , K0, ndim)[0,0]*ds(2))) # --> probleme
    Fy = fem.assemble_scalar(fem.form(sigma_amor(u,alpha, G , K0, ndim)[1,1]*ds(2))) # --> similaire à Rx (calculer en bas)
    forces[it_time] = np.array([t,Fx, Fy])
    print('(Fx, Fy): (%g, %g)'%(Fx,Fy))

    # r = ufl.inner( sigma_amor(u,alpha, G , K0, ndim), epsilon(v) ) * dx # reaction v : test function
    # linear_form = fem.form(r)
    # bb = fem.petsc.create_vector(linear_form)
    # Force = fem.petsc.assemble_vector(bb, linear_form)
    # Force.assemble()
    # Fy = np.sum(Force[facet_top_dofs[1]])
    # Fx = np.sum(Force[facet_top_dofs[0]])
    # forces[it_time] = np.array([t,Fx, Fy])
    # print('(Fx, Fy): (%g, %g)'%(Fx,Fy))

    nx = fem.Constant(msh, (1.0 ,0.0))
    ny = fem.Constant(msh, (0.0 ,1.0))
    ns = fem.Constant(msh, (0.0 ,1.0))
    tract_top_x = ufl.inner(sigma_amor(u,alpha, G , K0, ndim) * nx, ns)*ds(2)
    tract_top_y = ufl.inner(sigma_amor(u,alpha, G , K0, ndim) * ny, ns)*ds(2)
    Rx = fem.assemble_scalar(fem.form(tract_top_x))
    Ry = fem.assemble_scalar(fem.form(tract_top_y))
    React[it_time]=np.array([t,Rx, Ry])
    print('(Rx, Ry): (%g, %g)'%(Rx,Ry))


    # #Calculate individually the energy
    # elastic_energy       = 0.5*ufl.inner(sigma_amor(u,alpha, G , K0, ndim), epsilon(u))*dx
    # dissipated_energy    = Gc/cw*(w(alpha)/lw + lw * ufl.dot(ufl.grad(alpha), ufl.grad(alpha)))*dx
    # elastic_energy_scalar = fem.assemble_scalar(fem.form(elastic_energy))
    # surface_energy_scalar = fem.assemble_scalar(fem.form(dissipated_energy))

    # if it_time == 1 :
    #     surf_off_set = surface_energy_scalar
    #     #print('surf_offset_precrack : ',  surf_off_set)
    # energies[it_time]  = np.array([t,elastic_energy_scalar,surface_energy_scalar - surf_off_set])

    #     # monitor the results    
    # if comm.rank == 0:
    #     print( "\nalpha_max:", (alpha.x.array.max() ))
    #     print("\nIterationU and IterationAlpha: (%i,%i)"%(iter_u,iter_alpha))
    #     print("\nElastic and surface energies : (%g,%g)"%(elastic_energy_scalar,surface_energy_scalar - surf_off_set)) 



    #---- Save in VTK file
    # vtk0.write_function(u, t)
    # vtk1.write_function(alpha, t)
    xdmf0.write_function(u, t)
    xdmf1.write_function(alpha, t)
    xdmf3.write_function(sig_funct, t)
    xdmf4.write_function(eps_funct, t)
    # vtk4.write_function(s10_funct, t)
    # vtk5.write_function(e10_funct, t)



    #---Save Data in txt file
    np.savetxt(savedir + 'forces.txt'  , forces) # Fx/Fy
    np.savetxt(savedir + 'loading.txt'  , loading)
    np.savetxt(savedir + 'reaction.txt'  , React) #Rx/Ry

    
    it_time +=1


# Postprocessing

Data_forces = np.loadtxt(savedir+'forces.txt')
Data_load = np.loadtxt(savedir+'loading.txt')
Data_react = np.loadtxt(savedir+'reaction.txt')

plt.figure(1)
plt.plot(Data_load[:,1], Data_forces[:,2],'o', linewidth = 1, label='Fy')
plt.plot(Data_load[:,1], -Data_forces[:,1],'o', linewidth = 1, label='Fx')
plt.plot(Data_load[:,1], Data_react[:,1],'o', linewidth = 1, label='Rx')
plt.plot(Data_load[:,1], Data_react[:,2],'o', linewidth = 1, label='Ry')

plt.plot(Data_load[:,1], np.sqrt(Data_react[:,1]**2 +Data_react[:,2]**2),'o', linewidth = 1, label='sqrt(Rx²+Ry²)')
plt.xlabel('Displacement [mm]') ; plt.ylabel('Load [N]')
plt.legend()



plt.show()
